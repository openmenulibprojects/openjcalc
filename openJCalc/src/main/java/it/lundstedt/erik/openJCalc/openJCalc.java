package it.lundstedt.erik.openJCalc;
import it.lundstedt.erik.menu.*;

public class openJCalc
{

	public static void main( String[] args)
	{
		boolean doDebug;
		int first,second;
		boolean firstRun=true;
		//false keep
		//
		if (args.length < 1)
		{
			doDebug = false;
		}else {

			switch (args[0]) {
				case "true":
				case "TRUE":
					doDebug = true;
					break;
				case "false":
				case "FALSE":
					doDebug = false;
					break;
				default:
					doDebug = false;
			}
		}

		//



		Menu.licence("openCalc","version 0.0.1");

		Menu mainMenu=new Menu();

		String[] header={"Welcome","to","openCalc v0.0.1"};
		String[] items={
				"0)add         (+)",
				"1)subtract    (-)",
				"2)divide      (/)",
				"3)PI             ",
				"4)other constants",
				"5)multiply    (*)",
				"404)exit         "
		};
		/*set header and menuItems*/
		mainMenu.setHeader(header);
		mainMenu.setMenuItems(items);

		/*draw the menu*/
		mainMenu.drawMenu();
		/**/
		int choice=Menu.getChoice();
		//int choice=;

		switch (choice)
		{
			case 404:
			case 4:
			case 3:
				first=1;
				second=1;
			break;
			default:
				Menu.drawItem("what´s your first number?");
				first = Menu.getChoice();
				//int first=;
				Menu.drawItem("what´s your second number?");
				second = Menu.getChoice();
				//int second=;
		}
		System.out.println(list(choice,first,second,args,doDebug));
		exit(args,doDebug);
	}
	static boolean list (int choice,int first,int second,String[] args,boolean doDebug)
	{
		switch (choice)
		{
			case 0:
				Menu.drawItem(String.valueOf(Math.addExact(first,second)));
				break;
			case 1:
				Menu.drawItem(String.valueOf(Math.subtractExact(first,second)));
				break;
			case 2:
				if (first==0)
				{
					Menu.error("cant divide by "+first+" ,adding 1");first=1;
				}else if (second==0)
				{
					Menu.error("cant divide by "+second+" ,adding 1");second=1;
				}
				Menu.drawItem(String.valueOf(Math.floorDiv(first,second)));
				break;
			case 3:
				Menu.drawItem(String.valueOf(Math.PI));
				break;
			case 4:
				//WIP
				Menu.drawItem(String.valueOf(Math.PI));
				Menu.drawItem(String.valueOf(Math.E));

				Menu.drawItem(String.valueOf(Math.PI));
				Menu.drawItem(String.valueOf(Math.PI));
				Menu.drawItem(String.valueOf(Math.PI));

				break;
				case 5:
				Menu.drawItem(String.valueOf(Math.multiplyExact(first,second)));
				break;
			case 404:
				exit(args,doDebug);
		}
		return true;
	}

	public static void exit(String[] args,boolean doDebug)
	{
		if (args.length < 2)
		{
			String[] str = {String.valueOf(doDebug), "keep"};
			openJCalc.main(str);
		}
		if (args[1].equals("keep")) {
				Menu.debug("kept", doDebug);
				boolean doQuit = Utils.quit();
				if (doQuit == true) {
					System.exit(0);
				} else if (doQuit == false) {
					String[] str = {String.valueOf(doDebug), "keep", String.valueOf(false)};
					openJCalc.main(str);
				}
			} else {
				System.exit(0);
			}

	}
}